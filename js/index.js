function $(id) {
    return document.getElementById(id);
}

// Put event listeners into place
window.addEventListener("DOMContentLoaded", function () {
    // Grab elements, create settings, etc.
    var canvas = document.getElementById("canvas");
    available = true;
    picturesPos = [
        {x: 10, y: 10},
        {x: 325, y: 10},
        {x: 10, y: 248.75},
        {x: 325, y: 248.75}
    ];
    context = canvas.getContext("2d");
    context.fillStyle = 'white';
    context.fillRect(0, 0, 640, 490);
    // On dessine les emplacements des photos
//        for (var i = 0; i < picturesPos.length; i++) {
//            context.strokeRect(picturesPos[i].x,picturesPos[i].y,305,228.75);
//        }
//        context.strokeRect(10,10,305,228.75);
//        context.strokeRect(325,10,305,228.75);
//        context.strokeRect(10,248.75,305,228.75);
//        context.strokeRect(325,248.75,305,228.75);
    video = document.getElementById("video");
    videoObj = {"video": true};
    image_format = "jpeg";
    jpeg_quality = 95;
    errBack = function (error) {
        console.log("Video capture error: ", error);
    };

	navigator.mediaDevices.getUserMedia(videoObj)
	.then(function(stream) {
	  video.srcObject = stream;
      video.onloadedmetadata = (e) => {
        video.play();
      };
	})
	.catch(function(err) {
	  /* handle the error */
	});


    document.getElementById('body').addEventListener("keypress", function (e) {
        if (available) {
            if ((e.key == 'Enter') || (e.key == ' ')) {
                context.fillRect(0, 0, 640, 490);
                takePictures(0);
            }
        }
    });

    document.getElementById('body').addEventListener("click", function (e) {
        if (available) {
            context.fillRect(0, 0, 640, 490);
            takePictures(0);
        }
    });
}, false);

function takePictures(index) {
    available = false;
    countdown = document.getElementById('countdown');
    countdown.innerHTML = '5';
    setTimeout(function () {
        countdown.innerHTML = '4';
    }, 1000);
    setTimeout(function () {
        countdown.innerHTML = '3';
    }, 2000);
    setTimeout(function () {
        countdown.innerHTML = '2';
    }, 3000);
    setTimeout(function () {
        countdown.innerHTML = '1';
    }, 4000);
    setTimeout(function () {
        countdown.innerHTML = '';
        context.drawImage(video, picturesPos[index].x, picturesPos[index].y, 305, 228.75);
        if ((index + 1) < picturesPos.length) {
            takePictures(index + 1);
        } else {
            savePictures();
            available = true;
        }
    }, 5000);
}

function savePictures() {
    var dataUrl = canvas.toDataURL("image/jpeg", 0.85);
    var request = new XMLHttpRequest();
    request.open("POST", "camera_upload.php");
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.send("imgBase64=" + dataUrl);
    
    //var image = canvas.toDataURL("image/jpeg", 1).replace("image/jpeg", "image/octet-stream");

	//window.location.href=image;
}
